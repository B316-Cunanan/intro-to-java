package com.zuitt.example;

public class StaticPoly {
    //Static polymorphism - this is the ability to have
    public int addition(int a, int b){
        return a+b;
    }

    public int addition(int a, int b, int c){
        return a+b;
    }

    public double addition(double a, double b){
        return a+b;
    }
}
