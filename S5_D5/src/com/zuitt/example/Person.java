package com.zuitt.example;

public class Person implements Actions, Greetings{
    //For a class to implement or use an interface, we use the implements keyword


    public void sleep() {
        System.out.println("Zzzzzzzzz.....");

    }


    public void run() {
        System.out.println("Running on the road");
    }

    public void eat() {
        System.out.println("Munch munch!");
    }

    public void code() {
        System.out.println("type type type");
    }

    public void holidayGreet() {
        System.out.println("holidayGreet");
    }

    public void morningGreet() {
        System.out.println("morningGreet");
    }
}
