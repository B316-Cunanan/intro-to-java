package com.zuitt.example;
import java.util.ArrayList;

public class Contact {

    private String name;

    private ArrayList<String> phoneNumbers;

    private ArrayList<String> allAddress;

    public Contact (String name, String contactNumber, String address){
        this.name = name;
        this.phoneNumbers = new ArrayList<>();
        this.phoneNumbers.add(contactNumber);
        this.allAddress = new ArrayList<>();
        this.allAddress.add(address);
    }


    //Getters & Setters

    //Contact Number
    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    //Contact Number

    public void addPhoneNumber(String phoneNumber) {
        this.phoneNumbers.add(phoneNumber);
    }

    public ArrayList<String> getPhoneNumber() {
        return phoneNumbers;
    }

    public String getContactNumber(){
        return phoneNumbers.get(0);
    }

    public void setContactNumber(String contactNumber){
        this.phoneNumbers.set(0,contactNumber);
    }

    //Address

    public void addAllAddress(String allAddress) {
        this.allAddress.add(allAddress);
    }

    public ArrayList<String> getAllAddress() {
        return allAddress;
    }

    public String getAddress(){
        return allAddress.get(0);
    }

    public void setAddress(String address){
        this.allAddress.set(0, address);
    }

}
