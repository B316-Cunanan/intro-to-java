package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();


        //Adding contact infos
        Contact contact1 = new Contact("John Doe", "+639152468596", "Quezon City");
        contact1.addPhoneNumber("+639228547963");
        contact1.addAllAddress("Makati City");


        Contact contact2 = new Contact("Jane Doe", "+639162148573", "Caloocan City");
        contact2.addPhoneNumber("+639173698541");
        contact2.addAllAddress("Pasay City");

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        //Prints

        System.out.println(contact1.getName());
        System.out.println("----------------");
        System.out.println(contact1.getName() + " has the following registered numbers: ");
        for (String phoneNumber : contact1.getPhoneNumber()) {
            System.out.println(phoneNumber);
        }
        System.out.println("----------------");
        System.out.println(contact1.getName() + " has the following registered address:");
        System.out.println("my home in " + contact1.getAllAddress().get(0));
        System.out.println("my office in " + contact1.getAllAddress().get(1));


        System.out.println("=======================");

        System.out.println(contact2.getName());
        System.out.println("----------------");
        System.out.println(contact2.getName() + " has the following registered numbers: ");
        for (String phoneNumber : contact2.getPhoneNumber()) {
            System.out.println(phoneNumber);
        }
        System.out.println("----------------");
        System.out.println(contact2.getName() + " has the following registered address:");
        System.out.println("my home in " + contact2.getAllAddress().get(0));
        System.out.println("my office in " + contact2.getAllAddress().get(1));





    }

}
