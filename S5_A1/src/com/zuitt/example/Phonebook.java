package com.zuitt.example;
import java.util.ArrayList;

public class Phonebook {

    ArrayList<Contact> contacts = new ArrayList<Contact>();

    //Default Constructor
    public Phonebook(){

    }
    //Constructor
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }


    //Getter
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    //Setter
    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }


}
