package com.zuitt.example;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car();
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//        car1.make = "Veyron";
//        car1.brand = "Bugatti";
//        car1.price = 20000;
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);


        Car car2 = new Car();
//
//        car2.make = "Brio";
//        car2.brand = "Honda";
//        car2.price = 883000;
//        System.out.println(car2.brand);
//        System.out.println(car2.make);
//        System.out.println(car2.price);
//
//
        Car car3 = new Car();
//
//        car3.make = "Terra";
//        car3.brand = "Nissan";
//        car3.price = 2474000;
//        System.out.println(car3.brand);
//        System.out.println(car3.make);
//        System.out.println(car3.price);

        /*
            Mini-Activity:

            Create two new instances of the Car class and save it in a variable called car2 and car3 respectively.
            Access the properties of the instance and update its values.
                make = String
                brand = String
                price = int
             You can come up with your own values.
             Print the values of each property of the instance.
        */
        Driver driver1 = new Driver("Alejandro", 25);
//        System.out.println(driver1.name);

        car1.start();
        car2.start();
        car3.start();

        //property getters
        System.out.println(car1.getMake());
        System.out.println(car2.getMake());

        //property setters
        car1.setMake("Veyron");
        System.out.println(car1.getMake());

        car2.setMake("Innova");
        System.out.println(car2.getMake());

        //carDriver Getter
        System.out.println(car1.getCarDriver().getName());

        Driver newDriver = new Driver("Antonio", 21);
        car1.setCarDriver(newDriver);

        // Get name of new carDriver
        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getCarDriver().getAge());

        System.out.println(car1.getCarDriverName());

        /*
        * Mini-Activity
        * Create a new class called Animal with the following attributes
        * name- string
        * color - string
        *
        * Add constructors, getters and setters for the class
        * */


        Animals animal1 = new Animals("Clifford", "Red");
        animal1.call();

        Dog dog1 = new Dog();
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setName("Hachiko");
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setColor("Brown");
        System.out.println(dog1.getColor());

        Dog dog2 = new Dog("Miko", "Dark Brown", "Corgi");
        dog2.call();
        System.out.println(dog2.getName());

        System.out.println(dog2.getDogBreed());
        dog2.greet();

        User user1 = new User("Terrence Gaffud", 25, "tgaff@mail.com", "Quezon City" );
        user1.call();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = dateFormat.parse("10/06/2023");
            endDate = dateFormat.parse("14/06/2023");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Course course1 = new Course("MACQ004", "An Introduction to Java for career-shifters", 30, 500.00, startDate, endDate, user1);
        course1.call();





    }
}
