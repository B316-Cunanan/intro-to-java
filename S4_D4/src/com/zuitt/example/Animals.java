package com.zuitt.example;

public class Animals {
    private String name;
    private String color;

    public Animals(){

    }
    public Animals(String name, String color){
        this.name = name;
        this.color = color;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getColor(){
        return color;
    }

    public void setColor(String color){
        this.color = color;
    }

    public void call(){
        System.out.println("Hi! My name is " + this.name + ".");
    }
}
