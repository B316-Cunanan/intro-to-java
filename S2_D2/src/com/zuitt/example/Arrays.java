package com.zuitt.example;

import java.util.ArrayList;

import java.util.HashMap;

public class Arrays {
    public static void main(String[] args) {


        //Declaration
        int[] intArray = new int[5];
        //the [] indicates that this int data type should be able to hold multiple int values
        //"new" is a keyword used in non-primitive data types to tell Java to create said variables
        //This process is called "Instantation"
        //The integer value inside the [] on this side of the statement indicates the amount of the integer values the array can hold

        //Declaration with initialization
        int[] intArray2 = {100, 200, 300, 400, 500};

        //Multi-dimensional Array
        String[][] classroom = new String[3][3];

        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";

        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        ArrayList<String> students = new ArrayList<String>();

        //Adding elements ti the Arraylist
        students.add("John");
        students.add("Paul");

        //Accessing elements of the ArrayList
        students.get(0);

        //Adding an element in the ArrayList
        students.set(1, "George");

        //Removing a specific element in the ArrayList
        students.remove(1);

        //Remove all elements in the ArrayList
        students.clear();

        System.out.println(students.size());

        //Hashmaps
        HashMap<String, String>job_position = new HashMap<String, String>();

        //Adding elements into the HashMap
        job_position.put("Brandon", "Student");
        job_position.put("Alice", "Dreamer");

        //Accessing element of the HashMap
        job_position.get("Alice");
        System.out.println(job_position.get("Alice"));

        //Removing elements in HashMap
        job_position.remove("Brandon");
        System.out.println(job_position);

        //Getting the key of the elements of HashMaps
        System.out.println(job_position.keySet());

        //Operators allow us to manipulate the values that we store in variables. They represent logical, and arithmetic operations.

        /*
        Types of Operators

        1. Arithmentic: +, -, *, /, %
        2. Comparison: >, <, >=, <=, ==, !=
        3. Logical: &&, ||, !
        4. Assignment: =
         */
    }
}