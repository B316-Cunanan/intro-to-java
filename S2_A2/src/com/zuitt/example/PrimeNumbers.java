package com.zuitt.example;
import java.util.ArrayList;
import java.util.HashMap;


public class PrimeNumbers {
    public static void main(String[] args) {

        //Prime numbers
        int[] intArray = {2, 3, 5, 7, 11};

        System.out.println("The first prime number is: " +intArray[0]);

        System.out.println("The second prime number is: " +intArray[1]);

        System.out.println("The third prime number is: " + intArray[2]);

        System.out.println("The fourth prime number is: " + intArray[3]);

        System.out.println("The fifth prime number is: " + intArray[4]);

        //Array List
        ArrayList<String> friends = new ArrayList<String>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);


        //Hashmap
        HashMap<String, String>inventory = new HashMap<String, String>();

        inventory.put("toothpaste", "15");
        inventory.put("toothbrush", "20");
        inventory.put("soap", "12");

        System.out.println("Our current inventory consists of: " + inventory);



    }
}
