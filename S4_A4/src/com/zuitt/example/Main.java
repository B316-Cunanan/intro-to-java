package com.zuitt.example;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;

public class Main {
    public static void main(String[] args) {

        User user1 = new User("Terrence Gaffud", 25, "tgaff@mail.com", "Quezon City" );
        user1.call();
        System.out.println();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = dateFormat.parse("10/06/2023");
            endDate = dateFormat.parse("14/06/2023");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Course course1 = new Course("MACQ004", "An Introduction to Java for career-shifters", 30, 500.00, startDate, endDate, user1);
        course1.call();





    }
}
