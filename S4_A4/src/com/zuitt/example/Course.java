package com.zuitt.example;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Course {
    private String name;

    private String description;

    private int seats;

    private Double fee;
    private Date startDate;

    private Date endDate;

    private User instructor;

    public Course(){

    }
    public Course(String name, String description, int seats, Double fee, Date startDate, Date endDate, User instructor){

        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
        this.instructor = instructor;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public int getSeats(){
        return seats;
    }

    public void setSeats(int seats){
        this.seats = seats;
    }

    public Double getFee(){
        return fee;
    }

    public void setFee(Double fee){
        this.fee = fee;
    }

    public Date getStartDate(){
        return startDate;
    }

    public void setStartDate(Date startDate){
        this.startDate = startDate;
    }

    public Date getEndDate(){
        return endDate;
    }

    public void setEndDate(Date endDate){
        this.endDate = endDate;
    }

    public User getInstructor(){
        return instructor;
    }

    public void setInstructor(User instructor){
        this.instructor = instructor;
    }

    public void call(){
        System.out.println("Welcome to the course " + this.name + ". This course can be described as " + this.description + ". Your instructor for this course is " + this.instructor.getName() + ". Enjoy!");
    }
}

