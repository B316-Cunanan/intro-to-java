package com.zuitt.example;
import java.util.Scanner;

public class Leapyear {
    public static void main(String[] args) {

        Scanner obj1 = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year: ");
        int leapYear = obj1.nextInt();

        if (leapYear % 4 == 0 && (leapYear % 100 != 0 || leapYear % 400 == 0)) {
            System.out.println(leapYear + " is a leap year");
        }else {
            System.out.println(leapYear + " is NOT a leap year");
        }

    }
}
