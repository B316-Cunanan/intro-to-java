package com.zuitt.example;
import java.util.Scanner;

public class A1 {

    public static void main(String[] args) {
        Scanner obj1 = new Scanner(System.in);
        System.out.println("First Name: ");
        String firstName = new String(obj1.nextLine());

        Scanner obj2 = new Scanner(System.in);
        System.out.println("Last Name: ");
        String lastName = new String(obj2.nextLine());

        Scanner obj3 = new Scanner(System.in);
        System.out.println("First Subject Grade: ");
        double firstSubj = new Double(obj3.nextLine());

        Scanner obj4 = new Scanner(System.in);
        System.out.println("Second Subject Grade: ");
        double secondSubj = new Double(obj4.nextLine());

        Scanner obj5 = new Scanner(System.in);
        System.out.println("Third Subject Grade: ");
        double thirdSubj = new Double(obj5.nextLine());

        int x = (int) Math.floor((firstSubj + secondSubj + thirdSubj) / 3);


        System.out.println("Good day, " + firstName + " " + lastName +".");
        System.out.println("Your grade average is: " + x);

    }
}
