package com.zuitt.example;
import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Input an integer whose factorial will be computed: ");

        int num = 0;
        int ans = 1;
        int counterAns = 1;
        int ans2 = 1;


        try{
            num = in.nextInt();

            if (num <= 0) {
                System.out.println("Invalid input");
            } else {
                while (counterAns <= num) {
                    ans *= counterAns;
                    counterAns++;

                }
                System.out.println("The Factorial of " + num + " using WHILE LOOP is " + ans);


            }

            if (num <= 0) {
                System.out.println("Invalid input");
            } else {
                for (int counterAns2 = 1; counterAns2 <= num; counterAns2++) {
                    ans2 *= counterAns2;
                }
                System.out.println("The Factorial of " + num + " using FOR LOOP is " + ans2);
            }
        } catch (Exception e){
            System.out.println("Invalid input");
            e.printStackTrace();
        }

    }
}
